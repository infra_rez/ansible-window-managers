window-managers
=========

An ansible role to setup my window managers on Linux. The files folder hold all my dotfiles for various window managers. List of supported window maanagers below.

Other dot files can be found at; [ansible-dotfiles](https://gitlab.com/infra_rez/ansible-dotfiles)

### Currently supporting
- hyprland

`Hyprland note: In an effort to reduce the need for ansible modules, this role is just the config and does not install hyperland.`

##### Working last I checked. . .
 - sway

##### Out-of-Date
- Awesome

Role Variables
--------------

All variables which can be overridden are stored in vars/main.yml file as well as in table below.

| Variable                | Required | Default | Choices                   | Comments                                 |
|-------------------------|----------|---------|---------------------------|------------------------------------------|
| username                | yes      |         |                           | username                                 |
| awesome                 | no       | false   | true, false               | awesome window manager                   |
| awesome_rclua           | no       | true    | true, false               | create an awesome rc.lua file            |
| awesome_theme           | no       | true    | brandonia                 | create an awesome theme                  |
| sway                    | no       | false   | true, false               | sway windows manager                     |
| hyprland                |  no      | false   |  true, false              | hyprland window manager

Dependencies
------------

- hyprpaper
- wofi
- waybar

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:
```yml
- hosts: local
  roles:
    - window-managers
```

License
-------

Author Information
------------------

Brandon Steffe

**Email:** brandon@steffe.xyz

https://brandonia.xyz